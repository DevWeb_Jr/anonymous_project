from random import randint
from People.Person import Person


class Function():
    def __init__(self):
        pass
    
    def random_bool(self):
        return True if randint(0, 1) == 1 else False

    def ask_number(self):
        number = input("Choisir un nombre : ")
        try:
            number_int = int(number)
        except ValueError:
            number_int = self.ask_number()
        return number_int

    def ask_last_name(self):
        last_name = input("Nom : ")
        for i in last_name:
            if i.isdigit():
                self.ask_last_name()
        else:
            if self.validate():
                return last_name
            else:
                self.ask_last_name
    
    def ask_first_name(self):
        first_name = input("Prénom : ")
        for i in first_name:
            if i.isdigit():
                self.ask_first_name()
        else:
            if self.validate():
                return first_name
            else:
                self.ask_first_name
        
    def ask_gender(self):
        str = ""
        while str == "":
            answer = input("Homme => o \nFemme => n \n")
            if answer == "o" or answer == "O":
                if self.validate():
                    return True
            elif answer == "n" or answer == "N":
                if self.validate():
                    return False
            else:
                answer = self.ask_gender()
        

    def validate(self):
        validation = input("Valider => taper V \nModifier => taper M ")
        if validation == "v" or validation == "V":
            answer = True
        elif validation == "m" or validation == "M":
            answer = False
        else:
            answer = self.validate()
        return answer
    
    def ask_address(self):
        address = ""
        while address == "":
            address = input("Adresse : ")
        if self.validate():
            address = address
        else:
            address = self.ask_address()
    
    def ask_mail(self):
        mail = ""
        while mail == "":
            mail = input("Mail : ")
            # if ("@" and ".com") or ("@" and ".fr") in mail:
            if self.validate():
                mail = mail
            else:
                mail = self.ask_mail()
                
    def create_someone(self):
        # number = Function.ask_number()
        # for i in range(number):
        someone = Person(
                    last_name = self.ask_last_name(),
                    first_name = self.ask_first_name(),
                    gender = self.ask_gender(),
                    address = self.ask_address(),
                    mail = self.ask_mail(),
                )
        return someone
