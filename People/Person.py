from People.Human import Human
from faker import Faker

class Person(Human):
    """Une class héritée de l'objet Human, permettant l'instanciation d'objet Person"""
    def __init__(self, last_name, first_name, gender, address: str, mail: str, anonymous: bool=False):
        self.address = address
        self.mail = mail
        self.anonymous = anonymous
        Human.__init__(self, last_name, first_name, gender)

    """Une fonction pour rendre anonyme une personne"""
    def anonymise(self):
        if self.anonymous:
            self.anonymous = False

    """Une fonction qui vérifie si l'objet Person est anonyme et retourne le détail en affichage"""
    def is_anonymous(self):
        if self.anonymous:
            details = f"Cette personne semble vouloir rester anonyme."
            return True, details
        else:
            details = f"{self.last_name} {self.first_name} ({self.gender})"
            return False, details

    """Une fonction qui retourne le mail en str"""
    def get_mail_str(self):
        mail_str = f"{self.last_name}{self.first_name}@hotmail.com"
        return mail_str.lower()

    """Une fonction qui affiche le genre selon la valeur du booléen"""
    def get_gender_str(self):
        if self.gender:
            gender_str = "Homme"
        else:
            gender_str = "Femme"
        return gender_str

    """Une fonction qui affiche les détails d'un objet Person"""
    def print_details(self):
        details = f"{self.last_name} {self.first_name}\n{self.get_gender_str()}\n{self.get_mail_str()}\n{self.address}\n"
        print(details)

    """Une fonction qui modifie totalement les données d'un objet Person"""
    def hack(self):
        #générer des données alléatoires avec la librairie Faker
        fake = Faker()
        if self.is_anonymous == True:
            print("Impossible de hacker cet individu.")
        else:
            #si la personne est un homme
            if self.gender:
                #elle devient une femme
                self.gender = False
                #son nom change
                self.last_name = fake.last_name()
                #son prénom devient un prénom féminin
                self.first_name = fake.first_name_female()
                #son adresse change
                self.address = fake.address()
                #son mail change
                """self.mail = fake.ascii_free_email()"""
                self.mail =f"{self.first_name.lower()}{self.last_name.lower()}@hacker.com\n"
            else:
                #si la personne est un femme
                #elle devient un homme
                self.gender = True
                #son nom change
                self.last_name = fake.last_name()
                #son prénom devient un prénom féminin
                self.first_name = fake.first_name_male()
                #son adresse change
                self.address = fake.address()
                #son mail change
                """self.mail = fake.ascii_free_email()"""
                self.mail =f"{self.first_name}{self.last_name}@hacker.com\n"
