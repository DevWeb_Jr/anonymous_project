from People.Person import Person


if __name__ == '__main__':
    ##Création d'objets Person() avec en parametres un nom, un prénom, une adresse, et un mail
    stagiaire_01 = Person("Worries", "Jason", True, "3 rue de quelque part", "allaye@hotmail.com", False)
    stagiaire_02 = Person("The Bloody Doll", "Chucky", True, "5 rue de autre part", "javier@hotmail.com", False)
    stagiaire_03 = Person("Krueger","Freddy", True, "73 rue de un endroit","mickäel@hotmail.com", False)
    stagiaire_04 = Person("Wise", "Penny", True, "52 rue de un autre endroit","labib@hotmail.com", False)

    #créattion d'une liste contenant les objets Person
    people = [stagiaire_01,stagiaire_02,stagiaire_03,stagiaire_04]


    # premier test avec une boucle for
    #pour chaque stagiaire on affiche les détails
    for person in people:
        person.print_details()

    # second test avec une boucle for
    # on utilise la fonction hack() de la classe Person() sur chaque objet Person()
    for person in people:
        person.hack()
        person.print_details()


